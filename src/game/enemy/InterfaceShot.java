package game.enemy;

/**
 * Class with the public methods for {@link Shot}.
 *
 */
public interface InterfaceShot {

    /**
     * Method to set shot's direction.
     * @param dir {@link DirEnemy}.
     */
    void setDir(DirEnemy dir);
}
