L'applicazione SpaceWar 2.0 realizza un remake del primo videogioco della storia, SpaceWar.  
Per rinnovare un pò le meccaniche di gioco abbiamo aggiunto una modalità singleplayer in cui il giocatore alal guida della sua navicella deve eliminare ondate di nemici alieni.  
Abbiamo inoltre inserito ostacoli e powerUp e permesso ad alcuni nemici di seguire il player.  
La modalità multiplayer è invece classica con l'aggiunta dei soli powerUp.  
C'è la possibilità di controllare i 10 migliori score ottenuti su un dato pc. (Gli score sono salvati automaticamente)  